// Created for: Naturalis Biodiversity Center
// Created by: Marten Vijn & Titus Kretzschmar
// Date: Jan 2019
// License: CC BY-SA 4.0

// Voor de verschillende maten Phoenix Header hoeft hieronder alleen
// het aantal aders van de plug te worden aangegeven. 
pho_a=8; // aantal aders Phoenix plug

// diktes
plate_d=2; // dikte plaat
plate_dd=5; // dikte randje
plate_ddd=8; // dikte rim

// base plate
plate_t=131; // breedte bovenkant
plate_m=132; // breedte breedste punt
plate_b=131; // breedte onderkant
plate_h=55; // hoogte
plate_hm=36; // afstand tussen onderkant en breedste punt
 $fn=50;

// top-layer plate
plate2_t=131-4; // breedte bovenkant
plate2_m=131-3; // breedte breedste punt
plate2_b=131-6; // breedte onderkant
plate2_h=55-4; // hoogte
plate2_hm=36; // afstand tussen onderkant en breedste punt
 $fn=50;

// USB B frontpanel mount
usb_g=12; //stekker gat
usb_sg_d=3.2; //schroef gat diameter
usb_sg_l=10; //schroef gat lente
usb_sg_a=15; //afstand centrum schroefgat tot centrum schroefgat
usb_rim_d=5; //dikte omranding stekker gat
usb_rim=usb_g+usb_rim_d; //afmeting (x,y) omranding

// Phoenix Contact Header
pho_w_d=4; //wand dikte
pho_d_h=20; // hoogte doorvoer
pho_d_w=pho_a*7.5+1.5; //breedte doorvoer
// pho_d_w=16; //breedte doorvoer 2
// pho_d_w=31; //breedte doorvoer 4
// pho_d_w=46; //breedte doorvoer 6
// pho_d_w=61; //breedte doorvoer 8
pho_sg_d=3.5; // diameter schoefgat
pho_sg_l=10; // lengte schoefgat
pho_sg_a=pho_d_w+7;
// pho_sg_a=23; // afstand tussen middelpunt gat links en gat rechts 2
// pho_sg_a=38; // afstand tussen middelpunt gat links en gat rechts 4
// pho_sg_a=53; // afstand tussen middelpunt gat links en gat rechts 6
// pho_sg_a=68; // afstand tussen middelpunt gat links en gat rechts 8

flens_plaat();

module base(thick){
    linear_extrude(height=thick,twist=0,slices=1){
    polygon([
        [-plate_b/2,0],
        [plate_b/2,0],
        [plate_m/2,plate_hm],
        [plate_t/2,plate_h],
        [-plate_t/2,plate_h],
        [-plate_m/2,plate_hm]
        ]);
    }
    }

module base2(thick){
    linear_extrude(height=thick,twist=0,slices=1){
    polygon([
        [-plate2_b/2,0],
        [plate2_b/2,0],
        [plate2_m/2,plate2_hm],
        [plate2_t/2,plate2_h],
        [-plate2_t/2,plate2_h],
        [-plate2_m/2,plate2_hm]
        ]);
    }
    }

module rim(){
   translate([usb_g+19.5,plate_h/2-2.5-usb_g/2,2]) cube([usb_rim,usb_rim,plate_d+usb_rim_d]);
    }

module plate(){
    union(){
        base(plate_d);
        translate ([0,2,0])base2(plate_dd);
        rim(plate_ddd);
            }
        }

// logo links onder
// module logo(){
//     translate([-60,5,7])
//     scale([0.2,0.2,1])
//     import("logo5mm.stl", convexity = 10);
//     }
    
// logo rechts boven    
module logo(){
    translate([57,40,7])
    scale([0.2,0.2,1])
    import("logo5mm.stl", convexity = 10);
    } 
 
 // usblogo     
module usblogo(){
    translate([40,15,4.7])
    scale([0.02,0.02,1])
    import("usblogo.stl", convexity = 10);
    }    

 // phologo     
module phologo(){
    translate([-30,14,4.7])
    scale([0.2,0.2,1])
    import("phologo.stl", convexity = 10);
    }   

// .stl files kunnen worden gemaakt op basis van .png bestanden
// Dit kan vanaf de commandline (linux) met het programma png23D
// Met het commando:
// png23d -v -o stl -w 100 -d 2 logo.png logo.stl    

module usb(){
    translate([-usb_g/2+40,plate_h/2-usb_g/2,-1]) cube([usb_g,usb_g,usb_g]);
    translate([usb_sg_a+40,plate_h/2,-1]) cylinder(d=usb_sg_d,h=usb_sg_l);
    translate([-usb_sg_a+40,plate_h/2,-1]) cylinder(d=usb_sg_d,h=usb_sg_l);
    }

module phoenix(){
   translate([-pho_d_w/2-20,plate_h/2-pho_d_h/2,-1]) cube([pho_d_w,pho_d_h,pho_d_h]);
   translate([pho_sg_a/2-20,plate_h/2+pho_d_h/2+4,-1]) cylinder(d=pho_sg_d,h=pho_sg_l);
   translate([-pho_sg_a/2-20,plate_h/2+pho_d_h/2+4,-1]) cylinder(d=pho_sg_d,h=pho_sg_l);
   }

module flens_plaat(){
    difference(){
        plate();
        usb();
        phoenix();
        logo();
        // usblogo();
        // phologo();
        }
    }
