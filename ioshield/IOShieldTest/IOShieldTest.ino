// Created for: Naturalis Biodiversity Center
// Created by: Frank Vermeij
// License: CC BY-SA 4.0

#define Inp_0 A0
#define Inp_1 A1
#define Inp_2 A2
#define Inp_3 A3
#define Inp_4 A4
#define Inp_5 A5
#define Inp_6 1
#define Inp_7 0

#define Led 13


void setup() {
  Serial.begin(115200);

  pinMode(Inp_0,INPUT_PULLUP);
  pinMode(Inp_1,INPUT_PULLUP);
  pinMode(Inp_2,INPUT_PULLUP);
  pinMode(Inp_3,INPUT_PULLUP);
  pinMode(Inp_4,INPUT_PULLUP);
  pinMode(Inp_5,INPUT_PULLUP);
  pinMode(Inp_6,INPUT_PULLUP);
  pinMode(Inp_7,INPUT_PULLUP);

  pinMode(Led,OUTPUT);
  digitalWrite(Led,LOW); // Led uit
}

void CheckInputs(void)
{
boolean MyLed;
  digitalWrite(Led,LOW); // Led uit

  if(digitalRead(Inp_0) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 1");  // Debug hulp
  }

  if(digitalRead(Inp_1) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 2");  // Debug hulp
  }
  if(digitalRead(Inp_2) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 3");  // Debug hulp
  }
  if(digitalRead(Inp_3) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 4");  // Debug hulp
  }
  if(digitalRead(Inp_4) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 5");  // Debug hulp
  }
  if(digitalRead(Inp_5) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 6");  // Debug hulp
  }
  if(digitalRead(Inp_6) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 7");  // Debug hulp
  }
  if(digitalRead(Inp_7) == LOW)
  {
    MyLed = true; // Led Flag aan
    digitalWrite(Led,HIGH); // Led aan

    // Keyboard output code hier <<<<
    // Delay(200) // eventueel nog een delay
    Serial.println("Input 8");  // Debug hulp
  }
}


void loop() {
  CheckInputs();
  delay(50);
}
